'use static';

require('dotenv').config()

const express = require('express');
const app = express();

const port = process.env.GSS_PORT || 3008;

app.use(express.static('public'));

app.listen(port, function (err) {
    if (err) {
        console.log('Error...');
    }
    console.log(`Server listening on port ${port}!`);
})