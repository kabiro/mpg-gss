// Constants used by the app
const BUILDING_MODEL_SCALE_FACTOR = 4.85;
const SENSOR_MODEL_SCALE_FACTOR = 1.0;
const CENTER_LON = 9.173520236519524;
const CENTER_LAT = 48.78025004836571;

let GssApp = (function (GssSetup) {

    const chart = {};
    const data = {};
    const czml = [];
    const models = [];
    const colors = [
        '0021EF', '002FEF', '003EEF', '004DEF', '005BEF', '006AEF', '0079EF', '0087EE', '0096EE', '00A4EE', '00B3EE', '00C1EE',
        '00D0EE', '00DEED', '00EDED', '00EDDF', '00EDD0', '00EDC1', '00EDB3', '00ECA4', '00EC95', '00EC87', '00EC78', '00EC69',
        '00EC5B', '00EC4C', '00EB3E', '00EB2F', '00EB20', '00EB12', '00EB03', '0AEB00', '18EA00', '27EA00', '35EA00', '44EA00',
        '52EA00', '60EA00', '6FE900', '7DE900', '8BE900', '9AE900', 'A8E900', 'B6E900', 'C4E900', 'D2E800', 'E1E800', 'E8E100',
        'E8D300', 'E8C400', 'E8B600', 'E7A800', 'E79900', 'E78B00', 'E77D00', 'E76E00', 'E76000', 'E65200', 'E64300', 'E63500',
        'E62700', 'E61900', 'E60B00', 'E60002'
    ];
    let currentValue = '';
    let cInt;

    const dataSource = new Cesium.CzmlDataSource();
    const viewer = new Cesium.Viewer('cesium', {
        timeline: true,
        // animation: false,
        homeButton: false,
        screenModePicker: false,
        navigationHelpButton: false,
        baseLayerPicker: false,
        geocoder: false,
        // sceneMode: Cesium.SceneMode.SCENE3D,
        imageryProvider: Cesium.createOpenStreetMapImageryProvider({
            url: 'https://a.tile.openstreetmap.org/'
        }),
    });
    const scene = viewer.scene;


    function init() {

        // Load app code
        GssSetup.sayHello();

        // Draw the color scale
        createColorScale(colors);

        // Adjustment of the building models
        let position = Cesium.Cartesian3.fromDegrees(CENTER_LON, CENTER_LAT, 0.0);
        let heading = Cesium.Math.toRadians(64.5);
        let pitch = 0.0;
        let roll = 0.0;
        let hpr = new Cesium.HeadingPitchRoll(heading, pitch, roll);
        let orientation = Cesium.Transforms.headingPitchRollQuaternion(position, hpr);

        // Array with 3d models
        models.push({
            id: 'bau-1-0',
            name: 'Bau 1 Floor 0',
            position: position,
            orientation: orientation,
            model: {
                uri: './models/converted/hft/hft_bau_1_0.gltf',
                scale: BUILDING_MODEL_SCALE_FACTOR
            },
        },
            {
                id: 'bau-1-1',
                name: 'Bau 1 Floor 1',
                position: position,
                orientation: orientation,
                model: {
                    uri: './models/converted/hft/hft_bau_1_1.gltf',
                    scale: BUILDING_MODEL_SCALE_FACTOR
                },
            },
            {
                id: 'bau-1-2',
                name: 'Bau 1 Floor 2',
                position: position,
                orientation: orientation,
                model: {
                    uri: './models/converted/hft/hft_bau_1_2.gltf',
                    scale: BUILDING_MODEL_SCALE_FACTOR
                },
            },
            {
                id: 'bau-1-3',
                name: 'Bau 1 Floor 3',
                position: position,
                orientation: orientation,
                model: {
                    uri: './models/converted/hft/hft_bau_1_3.gltf',
                    scale: BUILDING_MODEL_SCALE_FACTOR
                },
            });

        // Event listener
        viewer.canvas.addEventListener('click', function (e) {

            // Create chart for the selected sensor
            if (viewer.selectedEntity) {
                console.log('=========>', GssSetup.fetchedSensorList);
                let did = function () {
                    console.log('=========>', GssSetup.fetchedSensorList);
                    for (let i = 0; i < GssSetup.fetchedSensorList.length; i++) {
                        console.log('====> ', GssSetup.fetchedSensorList[i].name);
                        console.log('====> ', viewer.selectedEntity.id);
                        if (GssSetup.fetchedSensorList[i].name === viewer.selectedEntity.id) {
                            console.log('found!');
                            createChart(GssSetup.fetchedObservationList[viewer.selectedEntity.id]);
                            return;
                        }
                    }
                }
                did();
                // DEBUG
                // console.log('selected: \t', viewer.selectedEntity || "");
                // console.log('selected:name => \t', viewer.selectedEntity.name || "");
                // console.log('selected:id => \t ', viewer.selectedEntity.id || "");
            }

            var mousePosition = new Cesium.Cartesian2(e.clientX, e.clientY);
            var ellipsoid = viewer.scene.globe.ellipsoid;
            var cartesian = viewer.camera.pickEllipsoid(mousePosition, ellipsoid);
            if (cartesian) {
                var cartographic = ellipsoid.cartesianToCartographic(cartesian);
                var longitudeString = Cesium.Math.toDegrees(cartographic.longitude); //.toFixed(2);
                var latitudeString = Cesium.Math.toDegrees(cartographic.latitude); //.toFixed(2);
                console.log('You clicked at [lng/lat] ' + longitudeString + ', ' + latitudeString);
            } else {
                console.log('');
            }
        })

        drawBuildings(models);

        // Initial positioning
        // viewer.camera.flyTo({
        //     destination: Cesium.Rectangle.fromDegrees(9.17072814, 48.77871941, 9.17400812, 48.78217284),
        //     // orientation: {
        //     //     heading: Cesium.Math.toRadians(0),
        //     //     pitch: Cesium.Math.toRadians(0),
        //     //     roll:  Cesium.Math.toRadians(0)
        //     // }
        // });
    }

    /**
     * Entities are added to the Cesium.js globe
     * @param {*} models 
     */
    function drawBuildings(models) {
        for (let i = 0; i < models.length; i++) {
            viewer.entities.add(models[i]);

            if (models.length === i + 1) {
                viewer.trackedEntity = viewer.entities.getById(models[i].id);
            }
        }
    }

    /**
     * The resulting CZML obj from setup.js is used to draw the sensor spheres on the globe.
     * @param {*} czml 
     * @param {*} sensors 
     */
    function drawSensors(czml, sensors) {
        
        var dataSourcePromise = Cesium.CzmlDataSource.load(czml);
        viewer.dataSources.add(dataSourcePromise);

        let dsLength = viewer.dataSources.length;
        let ds = viewer.dataSources.get(0);
        let dse = ds.entities;
        // let obj = ds.entities.getById('TMS-B01-R303');

        for (let i = 0; i < sensors.length; i++) {
            let obj = ds.entities.getById(sensors[i].name);
            obj.ellipsoid.radii = scaleProperty(obj.properties.tempScale, 1);
            obj.ellipsoid.material.color = colorProperty(obj.properties.tempScale, 1, obj.ellipsoid.material, colors);
        }
    }

    /**
     * Callback to change the sphere scaling according to the current value of the time-slider
     * @param {*} obj 
     * @param {*} scalingFactor 
     * @param {*} material 
     */
    function scaleProperty(obj, scalingFactor) {
        return new Cesium.CallbackProperty(function (time, result) {
            result = obj.getValue(time, result);
            result = result * 0.0633;
            return new Cesium.Cartesian3(result, result, result)
        }, obj.isConstant);
    }

    /**
     * Callback to change the sphere scaling according to the current value of the time-slider
     * @param {*} obj 
     * @param {*} scalingFactor 
     * @param {*} material 
     */
    function colorProperty(obj, scalingFactor, colors) {
        return new Cesium.CallbackProperty(function (time, result) {
            let colr = [
                '0021EF', '002FEF', '003EEF', '004DEF', '005BEF', '006AEF', '0079EF', '0087EE', '0096EE', '00A4EE', '00B3EE', '00C1EE',
                '00D0EE', '00DEED', '00EDED', '00EDDF', '00EDD0', '00EDC1', '00EDB3', '00ECA4', '00EC95', '00EC87', '00EC78', '00EC69',
                '00EC5B', '00EC4C', '00EB3E', '00EB2F', '00EB20', '00EB12', '00EB03', '0AEB00', '18EA00', '27EA00', '35EA00', '44EA00',
                '52EA00', '60EA00', '6FE900', '7DE900', '8BE900', '9AE900', 'A8E900', 'B6E900', 'C4E900', 'D2E800', 'E1E800', 'E8E100',
                'E8D300', 'E8C400', 'E8B600', 'E7A800', 'E79900', 'E78B00', 'E77D00', 'E76E00', 'E76000', 'E65200', 'E64300', 'E63500',
                'E62700', 'E61900', 'E60B00', 'E60002'
            ]
            colr.reverse();

            result = obj.getValue(time, result);

            // Calculation of the color for the temperature value
            result = parseInt(result.toFixed(2) * 100);
            let alphaHex = '0xFF';
            let v = 100 / 1500 * (result - 1500);
            v = v >= 0 ? v : 0;
            let idx = 64 / 100 * v
            idx = Math.round(idx);
            rgba = alphaHex + colr[idx];

            currentValue = result / 100;

            return Cesium.Color.fromRgba(rgba)
        }, false);
    }

    /**
     * Create the highchart chart and use the observation data from STA to display a line chart.
     * @param {*} observations 
     */
    function createChart(observations) {
        let ar = [];
        for (var i = 0; i < observations.length; i++) {
            ar.push([new Date(observations[i][3]), observations[i][2]]);
            // ar.push([new Date(observations[i].resultTime), observations[i].result]);
        }

        console.log(ar);
        Highcharts.stockChart('chart-container', {
            rangeSelector: {
                selected: 1
            },

            title: {
                text: 'AAPL Stock Price'
            },

            series: [{
                name: 'AAPL',
                data: ar,
                tooltip: {
                    valueDecimals: 2
                }
            }]
        });
    }


    /**
     * Developer Console Log
     * To verify the contentens of different objects.
     * Not used on the frontend
     */
    function logAll() {

        console.log('=== Log =============================');
        console.log('');
        console.log('>>> viewer');
        console.log(viewer);
        console.log('');
        console.log('>>> viewer.entities');
        console.log(viewer.entities);
        console.log('');
        console.log('>>> viewer.entities.values :length ' + viewer.entities.values.length);
        console.log(viewer.entities.values);
        console.log('');
        console.log('>>> List entities');
        for (let i = 0; i < viewer.entities.values.length; i++) {
            console.log(i + '\t: ' + viewer.entities.values[i].name);
            if (i === 1) {
                viewer.entities.values[i].show = !viewer.entities.values[i].show
            }
        }
        console.log('');
        console.log('>>> viewer.trackedEntity :name ' + (viewer.trackedEntity ? viewer.trackedEntity.name : 'no entity set'));
        console.log(viewer.trackedEntity);
        console.log('');
    }

    /**
     * Function to place the model on the globe.
     * Called by the HTML buttons.
     * @param {*} id 
     */
    function placeModel(id) {
        console.log('id:' + id);
        if (typeof viewer.entities.getById(id) !== 'undefined') return;
        switch (id) {
            case 'bau-1-0':
                viewer.trackedEntity = viewer.entities.add(models[0]);
                break;
            case 'bau-1-1':
                viewer.trackedEntity = viewer.entities.add(models[1]);
                break;
            case 'bau-1-2':
                viewer.trackedEntity = viewer.entities.add(models[2]);
                break;
            case 'bau-1-3':
                viewer.trackedEntity = viewer.entities.add(models[3]);
                break;
            default:
                drawBuildings(models);
        }
        console.log('creating entity for:' + id);
    }

    /**
     * Function to remove the model from the globe.
     * Called by the HTML buttons.
     * @param {*} id 
     */
    function removeModel(id) {
        if (!id) {
            viewer.entities.removeAll();
        }
        switch (id) {
            case 'bau-1-0':
                viewer.trackedEntity = viewer.entities.removeById(models[0].id);
                break;
            case 'bau-1-1':
                viewer.trackedEntity = viewer.entities.removeById(models[1].id);
                break;
            case 'bau-1-2':
                viewer.trackedEntity = viewer.entities.removeById(models[2].id);
                break;
            case 'bau-1-3':
                viewer.trackedEntity = viewer.entities.removeById(models[3].id);
                break;
        }
        console.log('creating entity for:' + id);
    }

    /**
     * Function to toggle the visibility of the model on the globe.
     * The entity property 'show' of the viewer object is used.
     * @param {*} id 
     */
    function hideModel(id) {
        if (!id) {
            for (let i = 0; i < viewer.entities.values.length; i++) {
                viewer.entities.values[i].show = !viewer.entities.values[i].show
            }
            console.log('Visibility changed for ' + viewer.entities.values.length + ' entities');
            return;
        }
        viewer.entities.getById(id).show = !viewer.entities.getById(id).show;

        let matcher = {
            "bau-1-0": ['TMS-B01-R011', 'TMS-B01-R013', 'TMS-B01-R020', 'TMS-B01-R024', 'TMS-B01-R028'],
            "bau-1-1": ['TMS-B01-R106', 'TMS-B01-R108'],
            "bau-1-2": ['TMS-B01-R211'],
            "bau-1-3": ['TMS-B01-R303'],
        }
        if (viewer.dataSources.get(0)) {
            for (let i = 0; i < matcher[id].length; i++) {
                let idx = matcher[id][i];
                viewer.dataSources.get(0).entities.getById(idx).show = !viewer.dataSources.get(0).entities.getById(idx).show;
            }
        }
    }

    /**
     * Fetches the things, datastream and observations of all sensors.
     */
    function fetchData() {

        // Empty the list
        console.log('Start fetching (clear list beforehand)')
        GssSetup.clearSensors();

        let start = new Date().getTime();
        let waiting = false;
        let elemIcon1,
            elemText1,
            elemIcon2,
            elemText2,
            elemIcon3,
            elemText4,
            children,
            child,
            txt;

        // Things
        elemIcon1 = document.getElementById('l-things-icon').classList;
        elemText1 = document.getElementById('l-things-text');
        // Datastream
        elemIcon2 = document.getElementById('l-datastreams-icon').classList;
        elemText2 = document.getElementById('l-datastreams-text');
        // Sensors
        elemIcon3 = document.getElementById('l-sensors-icon').classList;
        elemText3 = document.getElementById('l-sensors-text');

        // Load things...
        console.log('Things...');
        GssSetup.getThings();
        switchIconText(elemIcon1, elemText1, true, ['fa-square', 'fa-spinner', 'fa-spin'], 'Things: loading...');
        setTimeout(function () {
            console.log(GssSetup.fetchedSensorList.length)
            if (GssSetup.fetchedSensorList.length === 0) {
                console.log('Something went wrong!');
                switchIconText(elemIcon1, elemText1, false, ['fa-spinner', 'fa-spin', 'fa-times'], 'Things: error.');
                return;
            };
            switchIconText(elemIcon1, elemText1, false, ['fa-spinner', 'fa-spin', 'fa-check-square'], 'Things: done.');
            console.log('...done.');

            // Load datastreams...
            console.log('Datastreams...');
            GssSetup.loadDatastreams();
            switchIconText(elemIcon2, elemText2, true, ['fa-square', 'fa-spinner', 'fa-spin'], 'Datastreams: loading...');
            setTimeout(function () {
                switchIconText(elemIcon2, elemText2, false, ['fa-spinner', 'fa-spin', 'fa-check-square'], 'Datastreams: done.');
                console.log('...done.');

                // Load sensors...
                console.log('Sensors...');
                GssApp.drawSensors(GssSetup.buildCZML(GssSetup.fetchedSensorList, GssSetup.fetchedObservationList), GssSetup.fetchedSensorList);
                switchIconText(elemIcon3, elemText3, true, ['fa-square', 'fa-spinner', 'fa-spin'], 'Sensors: loading...');
                setTimeout(function () {
                    switchIconText(elemIcon3, elemText3, false, ['fa-spinner', 'fa-spin', 'fa-check-square'], 'Sensors: done.');
                    console.log('...done.');
                }, 3000);
            }, 5000);
        }, 1000);
    }

    function switchIconText(icon, elem, isAnim, classes, text) {
        if (isAnim) {
            icon.remove(classes[0]);
            icon.add(classes[1]);
            icon.add(classes[2]);
        } else {
            icon.remove(classes[0]);
            icon.remove(classes[1]);
            icon.add(classes[2]);
        }
        let txt = document.createTextNode(text);
        elem.innerText = txt.textContent
    }

    /**
     * Function that draws the color scale on the ui.
     * @param {*} clr 
     */
    function createColorScale(clr) {
        let root = document.getElementById('c-color');
        let labelLeft = document.createElement('div');
        let labelRight = document.createElement('div');
        let labelCurrent = document.createElement('div');

        labelLeft.setAttribute('class', 'colr-label');
        labelLeft.innerText = document.createTextNode('15').textContent;
        labelRight.setAttribute('class', 'colr-label');
        labelRight.innerText = document.createTextNode('30').textContent;
        labelCurrent.setAttribute('class', 'colr-value');

        cInt = setInterval(function () {
            if (currentValue !== '') {
                labelCurrent.innerText = document.createTextNode(currentValue.toFixed(2)).textContent;
            }
        }, 100);

        root.appendChild(labelLeft);

        for (let i = 0; i < clr.length; i++) {
            let el = document.createElement('div');
            el.setAttribute('style', 'background-color: #' + clr[i] + ';');
            el.setAttribute('class', 'color-scale');
            root.appendChild(el);
        }

        root.appendChild(labelRight);
        root.appendChild(labelCurrent);
    }

    init();

    return {
        createChart,
        drawSensors,
        placeModel,
        removeModel,
        hideModel,
        logAll,
        fetchData
    }

})(GssSetup);