# Hochschule für Technik Stuttgart
Master PG  
GIS Studio SS2017  
Project: Visualize sensor data with Cesium.js and SensorThings API  

# npm (mandatory)

    npm install

https://nodejs.org/en/


# (if yarn)

    yarn

https://yarnpkg.com/en/


# Add Cesium (mandatory)

copy folder 
  from  `node_modules/cesium/Build/Cesium`
  to    `public/libs/Cesium` 


# run

    npm run start

http://localhost:3000/

### Doc | Webroot

File | public/sensors.json
> Contains the sensor description that is send to the SensorThings API (STA) endpoint

File | public/setup.js
> File contains functions to createSensors from sensor.json, insertObservations from csv files, getThings/getDatastreams/getObversations from STA and build the czml object

File | public/app.js
> File contains functions to drawSensors, place/remove/hideModels, createChart and fetch the inital data from STA

Folder | public/csv
> Contains the historical sensors data that was imported by using the STA.

Folder | public/models
> Contains the converted 3d models
